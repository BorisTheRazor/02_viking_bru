﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using Invector.vCharacterController;

public class FMODfootsteps : MonoBehaviour
{


    [FMODUnity.EventRef] public string footstepEvent;
    [FMODUnity.EventRef] public string landingEvent;

    FMOD.Studio.EventInstance eventInstance;
    vThirdPersonInput tpInput;
    vThirdPersonController tpController;

    RaycastHit rh;
    public LayerMask lm;
    public float Material = 1f;
    public float LocomotionType = 0f;




    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
    }

    void footstep()

    {
        if (tpInput.cc.inputMagnitude > 0.1)
        {
            MaterialCheck();

            eventInstance = RuntimeManager.CreateInstance(footstepEvent);
            RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
            eventInstance.setParameterByName("LocomotionType", LocomotionType);

            if (tpController.isSprinting) LocomotionType = 1f;
            else LocomotionType = 0f;

          
            eventInstance.setParameterByName("Material", Material);
            eventInstance.start();
            eventInstance.release();
        }

        

    }


    void landing()

    {
        MaterialCheck();

        eventInstance = RuntimeManager.CreateInstance(landingEvent);
        RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
        eventInstance.setParameterByName("Material", Material);
        eventInstance.start();
        eventInstance.release();
    }

    void MaterialCheck()

    {
        if (Physics.Raycast(transform.position, Vector3.down, out rh, 0.1f, lm))
        {
            Debug.Log(rh.collider.tag);

            if (rh.collider.tag == "Wood") Material = 0f;
            else if (rh.collider.tag == "Snow") Material = 1f;
            else if (rh.collider.tag == "Water") Material = 2f;
            else Material = 1f;
        }
    }


}
