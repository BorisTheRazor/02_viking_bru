﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHeightWind : MonoBehaviour
{

    [FMODUnity.EventRef] public string windEvent;
    FMOD.Studio.EventInstance windInstance;
    public GameObject cameraObject;

    // Start is called before the first frame update
    void Start()
    {

        windInstance = FMODUnity.RuntimeManager.CreateInstance(windEvent);
        windInstance.start();
        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(cameraObject.transform.position.y);
        windInstance.setParameterByName("CameraHeight", cameraObject.transform.position.y);
    }
}
