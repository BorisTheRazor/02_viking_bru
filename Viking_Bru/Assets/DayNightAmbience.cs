﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightAmbience : MonoBehaviour
{

    public DayNightController dnc;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FMODUnity.RuntimeManager.StudioSystem.setParameterByName("DayNight", dnc.currentTime);
    }
}
